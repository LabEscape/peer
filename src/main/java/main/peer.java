package main;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.net.InetAddress;
import java.net.MalformedURLException;

public class peer extends AbstractHandler
{
	public static String UserName;
	public static int myPort;
	public static String ipRange = "10.132.0.";
	public static int basePort   =  1;
	public static int topPort    = 20;
	public static int difficulty =  5;
	public static List<Integer> neighbours          = new    ArrayList<Integer>();
	public static ArrayList<chainBlock> blockchain  = new ArrayList<chainBlock>(); 
	public static PrivateKey privKey;
    public static PublicKey  pubKey;
    public static JSONObject keys            = new JSONObject();
    public static JSONObject ports           = new JSONObject();
    public static JSONObject reputation      = new JSONObject();
    public static Scanner inp = new Scanner(System.in);  
    
    public static chainBlock waitingBlock;
    public static JSONObject stakemultiplier = new JSONObject();
    public static List<String> bannedBlocks  = new     ArrayList<String>();

	@Override
	public void handle(String target, Request request, HttpServletRequest args, HttpServletResponse response) throws IOException, ServletException 
	{
		String contents = request.getReader().readLine();
		JSONObject Input = new JSONObject(contents);
		
		if(Input.has("hash") && reputation.getInt(Input.getString("author")) > 0) 
		{
			chainBlock newBlock = new chainBlock(Input.getString("data"),Input.getString("previousHash"),Input.getLong("timeStamp"),Input.getString("hash"),Input.getInt("nonce"),Input.getString("author"),Input.getString("signature"));	
			blockchain.add(newBlock);
			
			try 
			{
				if(!isChainValid(blockchain)) 
				{
					System.out.println("Block is not valid!");
					Signature sign2 = Signature.getInstance("SHA256withDSA");
					byte publicKeyData[] = Base64.getDecoder().decode( keys.getString(newBlock.author).getBytes());
					X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyData);
					KeyFactory kf = KeyFactory.getInstance("DSA");
					PublicKey publicKey = kf.generatePublic(spec);
					sign2.initVerify(publicKey);
					sign2.update(newBlock.hash.getBytes());
				
					if(sign2.verify(Base64.getDecoder().decode(newBlock.signature))) 
					{
						blacklist(newBlock.author);
					}
					blockchain.remove(blockchain.size()-1);
				}
				
				else 
				{
					String[] array = newBlock.data.split("-", -1);
					System.out.println(array[0]);
					
					int blockValue = reputation.getInt(newBlock.author) + 1;
					reputation.put(newBlock.author, blockValue);
					
					if(array[0].equals("Vouch"))
					{
						if(reputation.getInt(newBlock.author) > 0 && !newBlock.author.equals(array[1])) 
						{
							int newValue = reputation.getInt(array[1]) + reputation.getInt(newBlock.author)/5;
							reputation.put(array[1], newValue);
						}
					}
					
					else if(array[0].equals("Blacklist")) 
					{
						if(reputation.getInt(newBlock.author) >= 12 || newBlock.author.equals(UserName)) 
						{
							reputation.put(array[1], -10);
						}
					}
				}
			}
			catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		else if(Input.has("port")) 
		{
			if(!keys.has(Input.getString("userId"))) 
			{
				keys.put(Input.getString("userId"), Input.get("signature"));
				reputation.put(Input.getString("userId"), 9);
				try 
				{
					hello();
				} 
				catch (InterruptedException e1) 
				{}
			
				if(!Input.getString("userId").equals(UserName)) 
				{
					System.out.println("A new user has been registered into the network (" + Input.getString("userId") + "). Do you know this user? (Y/N)");
					String answer = inp.next();
					if( answer.equals("Y") || answer.equals("y") || answer.equals("yes")) 
					{
						try 
						{
							vouch(Input.getString("userId"));
						} 
						catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException | InvalidKeySpecException | JSONException | InterruptedException e) 
						{}
					}
				}
			}
		}
		
		else if(Input.has("chain")) 
		{
			JSONArray chain = new JSONArray();
			for(chainBlock b : blockchain) 
			{
				JSONObject block = new JSONObject();
				block.put("hash", b.hash);
				block.put("previousHash", b.previousHash);
				block.put("data", b.data);
				block.put("timeStamp", b.timeStamp);
				block.put("nonce", b.nonce);
				block.put("author", b.author);
				block.put("signature", b.signature);
				chain.put(block);
			}
			response.setContentType("application/json;charset=utf-8");
		    response.setStatus(HttpServletResponse.SC_OK);
		    response.getWriter().println(chain.toString());
		    request.setHandled(true);
		}
	}
	 
	public static void hello() throws IOException, InterruptedException
	{
		for(int i = basePort; i < topPort; i++) 
		{
		    if(InetAddress.getByName(ipRange + i).isReachable(300)) 
		    {
		    	URL url = new URL("http://" +ipRange + i + ":8000");
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Accept-Charset", "UTF-8");
				connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				byte[] out = ("{\"userId\":\"" + UserName + "\",\"port\":" + myPort  + ",\"signature\":\"" + new String (Base64.getEncoder().encode(pubKey.getEncoded()) , "UTF-8") + "\"}").getBytes();
				connection.connect();
				
				try 
				{
					OutputStream os = connection.getOutputStream();
			        os.write(out);
			        os.close();
			        InputStream response = connection.getInputStream();
				}
				catch(IOException e)
				{}
		    }
		}
	}
	
	public static void blacklist(String user) throws IOException, InterruptedException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, InvalidKeySpecException
	{
		Signature sign = Signature.getInstance("SHA256withDSA");
	    String log = "Blacklist-" + user;
	    	
		chainBlock newBlock =  new chainBlock(log,blockchain.get(blockchain.size()-1).hash, UserName);
		newBlock.mineBlock(difficulty);
		                            
		System.out.println("\nBlockchain is Valid: " + isChainValid(blockchain));
		System.out.println("Block created sucessfully\n");
			
		sign.initSign(privKey);
		byte[] bytes2 = newBlock.hash.getBytes();
		sign.update(bytes2);
		byte[] signature2 = sign.sign();
		    
		String signedHash2 = Base64.getEncoder().encodeToString(signature2);
		newBlock.signature = signedHash2;
		pushBlock(newBlock);
	}
	
	public static void vouch(String user) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnknownHostException, IOException, InvalidKeySpecException, InterruptedException 
	{
		Signature sign = Signature.getInstance("SHA256withDSA");
		String log = "Vouch-" + user;
		    	
		chainBlock newBlock =  new chainBlock(log,blockchain.get(blockchain.size()-1).hash, UserName);
		newBlock.mineBlock(difficulty);
			                            
		System.out.println("\nBlockchain is Valid: " + isChainValid(blockchain));
		System.out.println("Block created sucessfully\n");
				
		sign.initSign(privKey);
		byte[] bytes2 = newBlock.hash.getBytes();
		sign.update(bytes2);
		byte[] signature2 = sign.sign();
			    
		String signedHash2 = Base64.getEncoder().encodeToString(signature2);
		newBlock.signature = signedHash2;
		pushBlock(newBlock);
	}
	
	public static void pushBlock(chainBlock block) throws IOException, InterruptedException
	{
		for(int i = basePort; i < topPort; i++) 
		{
			URL url = new URL("http://" +ipRange + i + ":8000");
			if(InetAddress.getByName(ipRange + i).isReachable(300)) 
			{
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Accept-Charset", "UTF-8");
				connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
					 
				byte[] out = ("{\"hash\":\""         + block.hash         + 
					 		"\",\"previousHash\":\"" + block.previousHash + 
					 		"\",\"signature\":\""    + block.signature    + 
					 		"\",\"data\":\""         + block.data         +
					 		"\",\"timeStamp\":\""    + block.timeStamp    +
					 		"\",\"nonce\":"          + block.nonce        +
					 		",\"author\":\""         + block.author       +
				 			"\"}").getBytes(StandardCharsets.UTF_8);

				connection.connect();
				try 
				{
					OutputStream os = connection.getOutputStream();
					os.write(out);
					os.close();
					InputStream response = connection.getInputStream();
				}
				catch(IOException e)
				{}
			}
		}
	}
	
	public static void getChain() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException 
	{
		int prevReputation = 0;
		for(int i = basePort; i < topPort; i++) 
		{
			if(InetAddress.getByName(ipRange + i).isReachable(300)) 
			{
				URL url = new URL("http://" +ipRange + i + ":8000");
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Accept-Charset", "UTF-8");
				connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				byte[] out = ("{\"chain\":\"" + UserName + "\"}").getBytes(StandardCharsets.UTF_8);
				connection.connect();
				
				try 
				{
					OutputStream os = connection.getOutputStream();
			        os.write(out);
			        os.close();
				}
				catch(IOException e)
				{}
				
				int currentReputation = 0;
				Iterator<String> keys = ports.keys();
		        while(keys.hasNext()) 
		        {
		            String key = keys.next();
		            if(ports.getInt(key) == i) 
		            {
		            	currentReputation = reputation.getInt(key);
		            }      
		        } 
		        
				InputStream response = connection.getInputStream();
				String body = IOUtils.toString(response, "UTF-8");
				System.out.println(body);
				JSONArray Input = new JSONArray(body);
				ArrayList<chainBlock> newchain = new ArrayList<chainBlock>(); 
				
				for (int j = 0; j < Input.length(); j++) 
				{
				    JSONObject block = Input.getJSONObject(j);
				    chainBlock newBlock = new chainBlock(block.getString("data"),block.getString("previousHash"),block.getLong("timeStamp"),block.getString("hash"),block.getInt("nonce"),block.getString("author"),block.getString("signature"));
				    newchain.add(newBlock);
				}
				
				if(isChainValid(newchain) && newchain.size() > blockchain.size() && currentReputation > prevReputation) 
				{
					blockchain = newchain;
				}
				prevReputation = currentReputation;
			}
		}
	}
	
	public static void getUpToDate() 
	{
		for (chainBlock b : blockchain) 
		{
			if(b.data.equals("New User"))
			{
				reputation.put(b.author, 9);
			}
			
			String[] array = b.data.split("-", -1);
			
			int blockValue = reputation.getInt(b.author) + 1;
			reputation.put(b.author, blockValue);
			
			if(array[0].equals("Vouch"))
			{
				if(reputation.getInt(b.author) > 0 && !b.author.equals(array[1])) 
				{
					int newValue = reputation.getInt(array[1]) + reputation.getInt(b.author)/5;
					reputation.put(array[1], newValue);
				}
			}
			
			else if(array[0].equals("Blacklist")) 
			{
				if(reputation.getInt(b.author) >= 12 || b.author.equals(UserName)) 
				{
					reputation.put(array[1], -10);
				}
			}
		}
	}

	public static Boolean isChainValid( ArrayList<chainBlock> chain) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException 
	{
		chainBlock currentBlock; 
		chainBlock previousBlock;
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		Signature sign3 = Signature.getInstance("SHA256withDSA");
		
		for(int i=1; i < chain.size(); i++) 
		{
			currentBlock = chain.get(i);
			previousBlock = chain.get(i-1);
			
			byte publicKeyData[] = Base64.getDecoder().decode( ( keys.getString(currentBlock.author)).getBytes());
			X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyData);
			KeyFactory kf = KeyFactory.getInstance("DSA");
			PublicKey publicKey = kf.generatePublic(spec);
			sign3.initVerify(publicKey);
			sign3.update(currentBlock.hash.getBytes());
			
			if(!sign3.verify(Base64.getDecoder().decode(currentBlock.signature))) 
			{
				System.out.println("Signatures do not match");			
				return false;
			}
			
			if(!currentBlock.hash.equals(currentBlock.calculateHash()))
			{
				System.out.println("Current Hashes not equal");			
				return false;
			}

			if(!previousBlock.hash.equals(currentBlock.previousHash)) 
			{
				System.out.println("Previous Hashes not equal");
				return false;
			}

			if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) 
			{
				System.out.println("This block hasn't been mined");
				return false;
			}
		}
		return true;
	}
	
	public static void main(String args[]) throws Exception 
	{
		System.out.println("Initiating...");
		
		InetAddress inetAddress = InetAddress.getLocalHost();
		System.out.println("IP Address:- " + inetAddress.getHostAddress());
		
		System.out.println("Type the username:");
		UserName = inp.next();
		
		String[] s1 = inetAddress.getHostAddress().split(Pattern.quote("."));

		myPort = Integer.parseInt(s1[s1.length - 1]);
		s1[s1.length - 1] = "";
		ipRange =  String.join(".", s1);
		System.out.println("IP Range:- " + ipRange);
		
		System.out.println("Starting at port "+myPort+"\n");
		Server server = new Server(8000);
	    server.setHandler( new peer());	 
	    server.start();
	    
		System.out.println("Searching for current chain...");
		getChain();
		
		if (blockchain.size() > 0 )
	    {
	    	getUpToDate();
	    }
		
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("DSA");
	    keyPairGen.initialize(2048);
		KeyPair pair = keyPairGen.generateKeyPair();
	      
	    privKey = pair.getPrivate();
	    pubKey  = pair.getPublic();

	    ArrayList<String> names = new ArrayList<String>();
	    
	    Iterator<String> namesit = reputation.keys();
        while(namesit.hasNext()) 
        {
            String key = namesit.next();
            names.add(key);     
        }  
        
        while(names.contains(UserName)) 
        {
        	System.out.println("Username is already taken. Please choose another:");
    		UserName = inp.next();
        }
    
	    hello();
  
	    Signature sign = Signature.getInstance("SHA256withDSA");
	    sign.initSign(privKey);
	    
	    String prevHash = "0";
	    
	    if (blockchain.size() > 0 )
	    {
	    	prevHash = blockchain.get(blockchain.size()-1).hash;
	    }
	    
	    chainBlock birthBlock =  new chainBlock("New User",prevHash, UserName);
		birthBlock.mineBlock(difficulty);
	    byte[] bytes = birthBlock.hash.getBytes();
	    sign.update(bytes);
	    byte[] signature = sign.sign();
	    
	    String signedHash = Base64.getEncoder().encodeToString(signature);
	    birthBlock.signature = signedHash;
	    pushBlock(birthBlock);
		
	    int command = 1;
		while(command != 0) 
		{
			System.out.println("Select: 0 - exit; 1 - hello; 2 - Create Log; 3 - Print Chain; 4 - Blacklist; 5 - Vouch; 6 - List Users");
			command = inp.nextInt();
			
			if(command == 1)
			{
					System.out.println("Sending block to");
					hello();
					getChain();
			}
			
			if(command == 2) 
			{
				if(blockchain.size() == 0)
				{
					System.out.println("BlockChain Empty -> Creating Genisis Block...");
					chainBlock newBlock = new chainBlock("Genisis!","0", UserName);
					newBlock.mineBlock(difficulty);
				      
				    sign.initSign(privKey);
				    byte[] bytes2 = newBlock.hash.getBytes();
				    sign.update(bytes2);
				    byte[] signature2 = sign.sign();
				    
				    String signedHash2 = Base64.getEncoder().encodeToString(signature2);
				    newBlock.signature = signedHash2;
					pushBlock(newBlock);
				}
			
				else
				{
					System.out.println("Creating new Block. Introduce your log:");
					String log = inp.next();
	
					chainBlock newBlock =  new chainBlock(log,blockchain.get(blockchain.size()-1).hash, UserName);
					newBlock.mineBlock(difficulty);
				                            
					System.out.println("\nBlockchain is Valid: " + isChainValid(blockchain));
					System.out.println("Block created sucessfully\n");
					
					sign.initSign(privKey);
				    byte[] bytes2 = newBlock.hash.getBytes();
				    sign.update(bytes2);
				    byte[] signature2 = sign.sign();
				    
				    String signedHash2 = Base64.getEncoder().encodeToString(signature2);
				    newBlock.signature = signedHash2;
					pushBlock(newBlock);
				}
			}
			
			if(command == 3) 
			{
				System.out.println("Printing current chain:");
				int n = 0;
				Signature sign2 = Signature.getInstance("SHA256withDSA");
				for(chainBlock b : blockchain) 
				{
					byte publicKeyData[] = Base64.getDecoder().decode(( keys.getString(b.author)).getBytes());
					X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyData);
					KeyFactory kf = KeyFactory.getInstance("DSA");
					PublicKey publicKey = kf.generatePublic(spec);
					sign2.initVerify(publicKey);
					sign2.update(b.hash.getBytes());
					
					System.out.println("Block "+ n + ":");
					System.out.println("	Hash:          " + b.hash                                               );
					System.out.println("	Signature:     " + b.signature                                          );
					System.out.println("	Verified:      " + sign2.verify(Base64.getDecoder().decode(b.signature)));
					System.out.println("	Previous Hash: " + b.previousHash                                       );
					System.out.println("	Data:          " + b.data                                               );
					System.out.println("	TimeStamp:     " + b.timeStamp                                          );
					System.out.println("	Nonce:         " + b.nonce                                              );
					System.out.println("	Author:        " + b.author                                             );
					n++;
				}
			}
			
			if(command == 4) 
			{
				System.out.println("User to blacklist:");
				String User = inp.next();
				blacklist(User);
			}
			
			if(command == 5) 
			{
				System.out.println("User to vouch for:");
				String User = inp.next();
				vouch(User);
			}
			
			if(command == 6) 
			{
				System.out.println("List of Users:");
		        Iterator<String> it = reputation.keys();
		        while(it.hasNext()) 
		        {
		            String key = it.next();
		            System.out.println("User: " + key + "   Reputation: " + reputation.getInt(key));      
		        }   
			}
			
			if(command == 0) 
			{
				System.out.println("Shutting down...");
				server.destroy();
			}
			
			else
			{
				System.out.println("\n");
			}
		}
	    server.join();
	    inp.close();
	}
}