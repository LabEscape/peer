package stake;
import java.util.Date;


public class chainBlock implements java.io.Serializable
{
	public  String         hash;
	public  String previousHash;
	public  String    signature;
	public  String         data;
	public  String       author;
	public  long      timeStamp;
	public  int           nonce;

	public chainBlock(String data,String previousHash,String auth)
	{
		this.data         =                 data;
		this.previousHash =         previousHash;
		this.timeStamp    = new Date().getTime();
		this.hash         =      calculateHash();
		this.author       =                 auth;
	}
	
	public chainBlock(String data, String previousHash, long timestamp, String hash, int nonce,String auth, String sign)
	{
		this.data         =         data;
		this.previousHash = previousHash;
		this.timeStamp    =    timestamp;
		this.hash         =         hash;
		this.nonce        =        nonce;
		this.author       =         auth;
		this.signature    =         sign;
	}
	
	public String calculateHash() 
	{
		String cHash = stringUtil.applySha256( previousHash + Long.toString(timeStamp) + Integer.toString(nonce) + data + author);
		return cHash;
	}
	
	public void mineBlock(int difficulty) 
	{
		String target = new String(new char[difficulty]).replace('\0', '0'); //Create a string with difficulty * "0" 
		while(!hash.substring( 0, difficulty).equals(target)) 
		{
			nonce ++;
			hash = calculateHash();
		}
		System.out.println("Block Mined successfully: " + hash);
	}
}